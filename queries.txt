ALTER TABLE `user` ADD COLUMN `store_id` INT NULL AFTER `role_id`, ADD COLUMN `is_deleted` BOOLEAN DEFAULT 0 NULL AFTER `updated_by`;

ALTER TABLE `store` ADD COLUMN `is_deleted` BOOLEAN DEFAULT 0 NULL AFTER `updated_by`; 
import { Injectable } from '@angular/core';
import "rxjs/Rx"
import { Headers, Response, URLSearchParams } from "@angular/http";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { NotificationsService } from 'angular2-notifications';
import { environment } from '../../environments/environment';

import { AuthService } from './auth.service';
import { TemplateService } from './template.service';

@Injectable()
export class ApiService {
    public baseUrl = "";
    public url: string;
    public aRequest: any[] = [];
    defaultParam: any[] = [];
    bLoadType: boolean = false;
    public applyParam: boolean = false;
    constructor(
        public http: HttpClient,
        public router: Router,
        public authService: AuthService,
        public templateService: TemplateService,
        public notify: NotificationsService
    ) {
        this.baseUrl = environment.baseUrl;
    }
    create(apiUrl: string): ApiService {
        if (apiUrl.search(':') != -1) {
            var aUrl = apiUrl.split(':');
            if (aUrl.length >= 2) {
                aUrl.shift();
                for (var sPart of aUrl) {
                    sPart = sPart.replace('/', '');
                    this.defaultParam.push(sPart);
                }
            }
        } else if (this.defaultParam.includes('id') == false) {
            this.defaultParam.push('id');
        }
        this.url = this.baseUrl + '/api/' + apiUrl;
        return this;
    }

    public makeUrl(param: Object): string {
        let url = this.url;
        this.applyParam = false;
        for (let sPart of this.defaultParam) {
            if (typeof param[sPart] != 'undefined') {
                if (url.search('/:') == -1) {
                    url += '/' + param[sPart];
                } else {
                    url = url.replace(':' + sPart, param[sPart]);
                }
                delete param[sPart];
                this.applyParam = true;
            }
        }
        return url;
    }

    query(param: Object, onNext: (json: any, isSuccess: boolean, status: number) => void, isSecure: boolean = true, isAccessible: boolean = false): void {
        let url = this.makeUrl(param);
        this._callApi('query', url, param, isAccessible, isSecure, onNext);
    }

    get(param: Object, onNext: (json: any, isSuccess: boolean, status: number) => void, isSecure: boolean = true, isAccessible: boolean = false): void {
        let url = this.makeUrl(param);
        this._callApi('get', url, param, isAccessible, isSecure, onNext);
    }

    save(param: Object, onNext: (json: any, isSuccess: boolean, status: number) => void, isSecure: boolean = true, isAccessible: boolean = false): void {
        this.applyParam = false;
        this._callApi('post', this.url, param, isAccessible, isSecure, onNext);
    }

    update(id, param: Object, onNext: (json: any, isSuccess: boolean, status: number) => void, isSecure: boolean = true, isAccessible: boolean = false): void {
        this.applyParam = false;
        if (id) {
            var url = this.url + '/' + id;
            this._callApi('put', url, param, isAccessible, isSecure, onNext);
        }
    }

    delete(id, param: Object, onNext: (json: any, isSuccess: boolean, status: number) => void, isSecure: boolean = true, isAccessible: boolean = false): void {
        this.applyParam = false;
        if (id) {
            var url = this.url + '/' + id;
            this._callApi('delete', url, param, isAccessible, isSecure, onNext);
        }
    }

    cancelRequest() {
        for (let i in this.aRequest) {
            this.aRequest[i].unsubscribe();
        }
        this.toggleLoader(false, true);
    }

    toggleLoader(toggle: boolean, bAllow: boolean = false) {
        if (this.bLoadType || bAllow) {
            this.templateService.bLoader = toggle;
        }
    }

    _callApi(type: string, url: string, params: Object, isAccessible: boolean, isSecure: boolean, customCallback: (json: any, isSuccess: boolean, status: number) => void): void {
        var oService;
        if (url.search(':') != -1 && !this.applyParam) {
            var aUrl: string[] = url.split('/:');
            url = aUrl[0];

        }
        let sParams: string = "";
        if (Object.keys(params).length != 0 && params.constructor === Object) {
            sParams = JSON.stringify(params);
            if (type == 'query' || type == 'get') {
                sParams = "";
                for (var i in params) {
                    sParams += i + "=" + params[i] + "&";
                }
                sParams = sParams.slice(0, -1);
            }
        }

        let headers: any = {
            'Content-Type': 'application/json; charset=utf-8',
            'Access-Control-Allow-Origin': '*',
            'Is-Accessible': (isAccessible?'true':'false')
        };
        oService = this.http;
        let oRequest;
        let oObservable: Observable<Response>;

        switch (type) {
            case 'get':
            case 'delete':
            case 'head':
                oRequest = oService[type](url, { search: sParams, headers: headers });
                break;
            case 'post':
            case 'put':
            case 'patch':
                oRequest = oService[type](url, params, { headers: headers });
                break;
            case 'query':
                if (sParams != "") {
                    url += '?' + sParams;
                }
                oRequest = oService.get(url, { headers: headers });
                break;
        }

        ['post', 'put', 'delete'].filter((val) => {
            if (type == val) {
                this.bLoadType = true;
                return true;
            }
            return false;
        });
        this.toggleLoader(true);
        oObservable = oRequest.pipe(
            tap(data => {
                    console.log('fetch data successfully data: ' + JSON.stringify({ url: url, params: sParams }))
                }
            ),
            map((res: Response) => 
                <Response>res//.json()
            ));

        let oResponse = oObservable
            .subscribe(
            response => {
                customCallback(response, true, response.status);
                this.toggleLoader(false);
            },
            (error: any) => {
                this.toggleLoader(false);
                
                if (error == null && typeof error == 'undefined') {
                    return customCallback('Something went wrong', false, 500);
                }
                if (typeof error.message != 'undefined') {
                    return customCallback(error.message, false, 500);
                }
                let message: any = error.json();
                if (error.status == 401) {
                    this.cancelRequest();
                    this.notify.error('Error!', message.exceptionMessage);
                    this.authService.logout();
                    this.router.navigateByUrl('/login');
                }
                if (error.status == 403) {
                    this.cancelRequest();
                    this.notify.error('Error!', message.exceptionMessage);
                    this.router.navigateByUrl('/home');
                }
                if (error.status == 500) {
                    this.cancelRequest();
                    this.notify.error('Error!', message.error);
                }
                return customCallback(message, false, error.status);

            }
            );
        this.aRequest.push(oResponse);
    }

}
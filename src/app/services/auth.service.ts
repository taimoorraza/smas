import { Injectable } from '@angular/core';
import "rxjs/Rx";

@Injectable()

export class AuthService {
	public session: any;
	currentRoute: string;
	sessionName:string = "sm_session";

	constructor() { }
	// Define Status code contants
    public access = {
		OK: 200,
		// "we don't know who you are, so we can't say if you're authorized to access
		// this resource or not yet, please sign in first"
		UNAUTHORIZED: 401,
		// "we know who you are, and your profile does not allow you to access this resource"
		FORBIDDEN: 403
	}
    
	setSession(data): void{
		this.session = data;
		this.session['isLoggedIn'] = true;
		localStorage.setItem(this.sessionName+'_token', JSON.stringify(this.session.token));
		localStorage.setItem(this.sessionName, JSON.stringify(this.session));
	}

	setItem(key:string, val: any) {
		if(this.isLoggedIn()) {
			localStorage.setItem(key, val);
		}
	}

	setSessionField(key:string, data:any): void{
		this.checkSession();
		this.session[key] = data;
		localStorage.setItem(this.sessionName, JSON.stringify(this.session));	
	}

	getSession(val: any): any {
		this.checkSession();
		return (this.session && typeof this.session[val] != 'undefined') ? this.session[val] : null;
	}

	checkSession():void{
		if (!(this.session && typeof this.session[this.sessionName+'_token'] != 'undefined')) {
			this.session = JSON.parse(localStorage.getItem(this.sessionName));
		}
	}

 	activatedPos(): boolean{
 		this.checkSession();
 		return ((typeof this.session.pos !='undefined' && typeof this.session.store != 'undefined')? true: false);
 	}

	isLoggedIn(): boolean {
		this.checkSession();
		return (this.session && this.session.isLoggedIn) ? this.session.isLoggedIn : null;
	}

	isCashierLoggedIn(): boolean{
		this.checkSession();
		if(this.session && this.session.isLoggedIn){
			return (typeof this.session['cashier'] != 'undefined' && typeof this.session['cashier']['cashier_id'] != 'undefined')? true: false;
		}
		return false;
	}

	cashierLogout(): boolean {
		this.checkSession();
		if(this.session && this.session.isLoggedIn){
			delete this.session.cashier;
			this.setSession(this.session);
			return true;
		}
		return false;
	}

	// logout clear session and object
    logout(): void {
        localStorage.removeItem(this.sessionName);
        localStorage.removeItem(this.sessionName+'_token');
		localStorage.clear();
        this.session = null;
    }

	// check user has permission for route
	isAuthenticated(route: string): any {
		if (this.isLoggedIn()) {
			this.currentRoute = route;
			return this.access.OK;
		} else {
			return this.access.UNAUTHORIZED;
		}
	}

	// check user has permission on action
	isAuthorized(action: string): boolean {
		let permissions = this.getSession('permissions');
		if (this.currentRoute && typeof permissions[this.currentRoute] != 'undefined') {
			var aPermission = permissions[this.currentRoute].split(',');
			return aPermission.indexOf(action) != -1;
		}
		return false;
	}
}
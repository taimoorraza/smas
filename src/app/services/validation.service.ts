import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import * as moment from 'moment';

@Injectable()
export class ValidationService {
    static getValidatorErrorMessage(label: string, validatorName: string, validatorValue?: any) {
        let config = {
            'required': ((label != '')? label + ' is required':'Field is required'),
            'invalidEmailAddress': 'Enter valid email address',
            'invalidPassword': 'Enter valid password. Password must be at least 6 characters long, and contain a number.',
            'invalidConfirmPassword': 'Password does not match.',
            'invalidAlphabet': 'Enter only alphabet character in '+ label,
            'invalidNumber': 'Enter only numbers in '+ label,
            'invalidMobileNumber': 'Enter valid mobile number',
            'pattern': label + ' is incorrect',
            'invalidDate': ((label != '') ? label + ' is invalid' : 'Date is invalid'),
            'minlength': `Minimum length ${validatorValue.requiredLength}`,
            'maxlength': `Maximum length ${validatorValue.requiredLength}`,
            'minValue': ((label != '') ? label + ' must be greater than 0' : 'Value must be greater than 0'),
            'maxValue': ((label != '') ? label + `must be less than ${validatorValue.requiredLength}` : `Value must be less than ${validatorValue.requiredLength}`),
            'range': `Value must be equal to ${validatorValue.requiredMinLength} or less than ${validatorValue.requiredMaxLength} `,
            'invalidDuplicate': 'Option already selected',
            'out_of_stock': 'Insufficient stock',
            'invalid_date': 'Invalid Date',
            'zeroval': ((label != '') ? label + ' cannot be zero' : 'value cannot be zero'),
            'dupError': ((label != '') ? label + ' already selected' : 'Item already selected'),
            'weightInvalid': 'Weight must be greater than 0',
            'dateRangeValidate': ((label != '') ? label : 'Arrival date') + ' must be greater than date',
            'dateValidate': 'PastDate is not allowed. Select from the Calendar'
        };
        return config[validatorName];
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if(control.hasError('required') || (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == ""))){
            return null;
        }
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        }
        return { 'invalidEmailAddress': true };

    }

    static mobileValidator(control) {
        let mobFormat = /^(\+?\d{1,3}[- ]?)?\d{10,13}$/;
        if(mobFormat.test(control.value)){
            return null;
        }
        return { 'invalidMobileNumber': true };
    }

    static duplicateCodeValidator(data: any, new_code: string, dataIndex: number){
        for(var i=0; i<data.length; i++){
            if(data[i].code.toLowerCase().trim() == new_code.toLowerCase().trim() && i != dataIndex){
                return true;
            }
        }
        return false;
    }

    static duplicatISOCodeValidator(data: any, new_code: string, dataIndex: number, code: string){
        for(var i=0; i<data.length; i++){
            if(code == "code2"){
                if(data[i].iso_code_2.toLowerCase().trim() == new_code.toLowerCase().trim() && i != dataIndex){
                    return true;
                }
            }
            else if(code == "code3"){
                if(data[i].iso_code_3.toLowerCase().trim() == new_code.toLowerCase().trim() && i != dataIndex){
                    return true;
                }
            }
        }
        return false;
    }

    static duplicateNameValidator(data: any, new_Name: string, dataIndex: number){
        for(var i=0; i<data.length; i++){
            if(data[i].name.toLowerCase().trim() == new_Name.toLowerCase().trim() && i != dataIndex){
                return true;
            }
        }
        return false;
    }
    
    static duplicateEmailValidator(data: any, new_Name: string, dataIndex: number){
        for(var i=0; i<data.length; i++){
            if(data[i].email.toLowerCase().trim() == new_Name.toLowerCase().trim() && i != dataIndex){
                return true;
            }
        }
        return false;
    }
    
    static duplicatePhoneValidator(data: any, new_Name: string, dataIndex: number){
        for(var i=0; i<data.length; i++){
            if(data[i].phone.toLowerCase().trim() == new_Name.toLowerCase().trim() && i != dataIndex){
                return true;
            }
        }
        return false;
    }

    static customRequired(scenario){
        return (control) => {
            if(scenario != 'Create' || !control.touched){
                return null;
            }
            return (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == "") ?
               {"required": true} :
               null);
        }
    }

    static isBlank(value){
        return (typeof value == 'undefined' || !value)?true:false;
    }

    static isString(value){
        return (typeof value == 'string')?true:false;
    }

    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if ((/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/.test(control.value))) {
            return null;
        }
        return { 'invalidPassword': true };

    }

    static allowAlphabet(control){
        // if(control.hasError('required') || (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == ""))){
        //     return null;
        // }
        // console.log(control.value);
        if (/^[A-Za-z\s]+$/.test(control.value)) {
            return null;
        } 
        return { 'invalidAlphabet': true };
    }

    static allowNumeric(control){
        if(control.hasError('required') || (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == ""))){
            return null;
        }
        if (/^\d+$/.test(control.value)) {
            return null;
        } 
        return { 'invalidNumber': true };

    }

    static validateDate(control){
        if(control.hasError('required') || (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == ""))){
            return null;
        }
        /**
         * Date Validation
         * DD.MM.YYYY
         */
        let parsedIsoDate = moment(control.value , "DD/MM/YYYY").isValid();

        if(parsedIsoDate != false){
            return null;
        }
        return { 'invalidDate': true };
    }
    
    static confirmPassValidator(control) {
        if (control.hasError('required')) {
            return null;
        }
        let form = control.root;
        if (!ValidationService.isBlank(form) && !ValidationService.isBlank(form.controls) && form.controls.hasOwnProperty('password')) {
            if (form.controls['password'].value == "" || form.controls['password'].value == control.value) {
                return null;
            }
        }
        return { 'invalidConfirmPassword': true };
    }

    static resetForm(form){
        for(var name in form.controls) {
           form.controls[name].setValue('');
           form.controls[name].setErrors(null);
        }
    }

    static validateForm(form, notCheck: string[] = []){
        for(var name in form.controls) {
                form.controls[name].updateValueAndValidity();
                form.controls[name].markAsTouched();

        }

        return form.valid;
    }

    static hasWhiteSpace(control){
        if(/^\s*$/.test(control.value) == false){
            return null;
        }
        return { 'required': true };
    }

  static min(min: number, cMsg?: string){
    return (control): {[key: string]: any} => {
      if (this.isPresent(Validators.required(control))){
            return null;
      } 
      this.weightValidate(control);
      
      let v: number = control.value;
      let msg ;
      switch(cMsg){
          case 'zeroval':
            msg = { [cMsg]: true}
          break;
          default:
            msg = { 'minValue': { 'requiredLength': min, 'actualLength': v } };
      }
      
      return v >= min ? null :  msg;
    };
  }

  static weightValidate(control){
      if(control.value != 0){
          return null;
      }
      return { 'weightInvalid': true };
  }
  
  static max(max: number, cMsg?: string){
    return (control): {[key: string]: any} => {
      if (this.isPresent(Validators.required(control))){
            return null;
      } 
      
      let v: number = control.value;
      
      return v <= max ? null :  { 'maxValue': { 'requiredLength': max, 'actualLength': v } };
    };
  }

  static range(range: Array<number>, cMsg?: string) {
    return (control): {[key: string]: any} => {
      let v: number = Number(control.value);
      range[0] = Number(range[0]);
      range[1] = Number(range[1]);
      if(typeof cMsg != 'undefined'){
        if(v <= 0 || v > range[1] || range[1] <= 0 && cMsg == 'out_of_stock'){
            return {'out_of_stock': true}
        }
        if((range[0] > range[1]) && cMsg == 'invalid_date'){
                return {'invalid_date': true}
        }
      }
       if(control.hasError('required') || (ValidationService.isBlank(control.value) )){
            return null;
      } 



        return (v >= range[0] && v <= range[1] ? null : {'range': { 'requiredMinLength': range[0], 'requiredMaxLength': range[1]} });
        };
    }

    static dateRange(range: Array<number>, cMsg?: string) {
        return (control): {[key: string]: any} => {
            let v: number = control.value;
            if((range[0] > range[1]) && cMsg == 'invalid_date'){
                return {'dateRangeValidate': true}
            }
            if((range[0] > range[1]) && cMsg == 'min_date'){
                return {'dateValidate': true}
            }
        };
    }

    static dateValidate(date, gT?) {
            return(control) => {
                if(control.hasError('required') || (ValidationService.isBlank(control.value) )){
                        return null;
                } 
                let a: number;
                if (!(date)) return null;
                if(typeof gT != 'undefined' && isNaN(gT) === false)
                    a = moment(date.dateControl.value, 'DD/MM/YYYY').add(gT, 'days').valueOf();
                else
                    a = moment(date.dateControl.value, 'DD/MM/YYYY').valueOf();
                
                
                let b: number = moment(control.value, 'DD/MM/YYYY').valueOf();
                if((a > b)){
                    return {'dateRangeValidate': true}
                }
        }
    }

    static validateDupItem(group, index, prefix, pPrefix?) {
		return(control) => {
            if(control.hasError('required') || (ValidationService.isBlank(control.value) )){
                    return null;
            } 
			if (!(group)) return null;
			
            let keys: string[] = Object.keys(group.controls);
            let pArray: string[] = [];
            let newArray:string[] = [];
            pPrefix = typeof pPrefix != 'undefined' ? pPrefix : null;
            
            keys.forEach((x, i) => {
                if(x.substring(0, prefix.length) == prefix){
                    newArray.push(x);
                }
                if(pPrefix != null && x.substring(0, pPrefix.length) == pPrefix){
                    pArray.push(x);
                }
            })

			let len: number = newArray.length;

			if (!len) return null;

            let ctrlName = (prefix + index);
            let pCtrlName = pPrefix != null ? (pPrefix + index) : null;

			for (let i = 0; i < len; i++) {
                if(pCtrlName != null && pPrefix != null && typeof group.controls[pPrefix + i] != 'undefined' && typeof group.controls[pCtrlName] != 'undefined' && pCtrlName != pArray[i] && ctrlName != newArray[i] && control.value == group.controls[newArray[i]].value){
                    if(group.controls[pCtrlName].value == group.controls[pPrefix + i].value){
                        return {'dupError': true};
                    }
                } else if(pPrefix == null && ctrlName != newArray[i] && control.value == group.controls[newArray[i]].value) {
					return {'dupError': true};
				}
			}
		}
	}

    static isPresent(obj: any): boolean {
        return obj !== undefined && obj !== null;
    }
}
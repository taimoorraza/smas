import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

import { AuthService } from "./auth.service";

@Directive({
  selector: '[checkAuth]'
})

export class AuthDirective implements OnInit {
    public _permission = 'V';
    public el: HTMLElement;

    constructor(el: ElementRef, public auth: AuthService) { this.el = el.nativeElement; }

    @Input() set permission(perm: string){
        this._permission = perm || this._permission;
    }

    @Input('checkAuth') route: string;

    checkAuth(route: string, perm: string){
        let aPermissions: any[] = this.auth.getSession('permission');
        if(route != ''){
            return (typeof aPermissions[route] != 'undefined' && aPermissions[route].includes(perm));
        }
        return true;
    }

    checkAccess(route: string, perm: string){
        let bAccess: boolean = this.checkAuth(route,perm);
        if(!bAccess){
            this.el.style.display = 'none';
        }
    }

    ngOnInit(){
        this.checkAccess(this.route, this._permission);
    }
}
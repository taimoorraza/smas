import { Component, HostBinding, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from './services/auth.service';
import { ApiService } from './services/api.service';
import { TemplateService } from './services/template.service';
import { AsideComponent } from "./shared/aside.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    constructor(
      private notify: NotificationsService,
      public auth: AuthService,
      public api: ApiService,
      public router: Router,
      public state: TemplateService,
      public cd: ChangeDetectorRef) { }

    title: string = this.state.app.name;
    @ViewChild('aside') public aside: AsideComponent;
    
    public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: true,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: "visible"
    };

    authLogin(): boolean {
        let bLogin: boolean = this.auth.isLoggedIn();
        return bLogin;
    }

    openAside() {
        this.aside.toggleAside();
    }

    goBack = function () {
        window.history.back();
    }

    ngOnInit(){
    	
    }

}
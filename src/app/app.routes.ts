import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/authguard.service';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard]},
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  //{ path: 'error_page', component: ErrorComponent },
  //{ path: '**', component: NotFoundComponent }
];

export const routing = RouterModule.forRoot(routes);
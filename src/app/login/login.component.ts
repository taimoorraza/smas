import { Component, ViewChild, ElementRef, Renderer, OnInit  } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from './../services/auth.service';
import { ApiService } from './../services/api.service';
import { TemplateService } from './../services/template.service';
import { ValidationService } from './../services/validation.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
	loginForm: any;
    @ViewChild('userInput') userInput: ElementRef;
    @ViewChild('passInput') passInput: ElementRef;
    @ViewChild('btnNext') btnNext: ElementRef;

  	constructor(
  	  	public router: Router,
        public authService: AuthService,
        public state: TemplateService,
        public apiService: ApiService,
        public notify: NotificationsService,
        public renderer: Renderer,
        public formBuilder: FormBuilder
  	) {
          if(this.authService.isLoggedIn()){
              this.router.navigate(['/home']);
          }
  		this.loginForm = this.formBuilder.group({
            'email': ['', [Validators.required, Validators.minLength(5)]],
            'password': ['', [Validators.required, Validators.minLength(6)]],
            'forgotPassword': this.fPControl,

        });
  	 }
  	user: any = {
        email: null,
        password: null,
        forgotPassword: null,
        store: 1
    };

    bForget = false;
    stores = [];
    selectedIndex = 0;
    fPControl: FormControl = new FormControl('', []);
    title: string = 'Sign in with your ' + this.state.app.name + ' Account'

    focus(ele) {
        this.renderer.invokeElementMethod(ele.nativeElement, 'focus');
    }

  	prevStep(from = "") {
        this.selectedIndex = 0;
        if(from == 'forget'){
            this.fPControl.clearValidators();
            this.bForget = false;
        }
        //this.focus(this.userInput);
        this.title = 'Sign in with your ' + this.state.app.name + ' Account';
    }

    getStores(){
        this.state.bLoader = true;
        this.apiService.create('stores').query({}, (res, isSuccess) => {
            if (isSuccess) {
                this.stores = res;
                this.state.bLoader = false;
            }
        }, false);
    }

    login(user) {
        this.state.bLoader = true;
        if (!ValidationService.validateForm(this.loginForm)) {
            return false;
        }
        this.apiService.create('login').save(user, (res, isSuccess) => {
            if (isSuccess) {
            	if(res.id !== undefined){
                    this.user.id = res.id;
                    this.selectedIndex = 1;
                    this.state.bLoader = false;
                    this.title = 'Select store and Conitnue with your '+ this.state.app.name + ' Account';
                }
            }
        }, false);
    }

    continue(){
        this.state.bLoader = true;
        if(this.user.store === undefined || !this.user.store){
            this.notify.error('Error','Please select any store to continue');
            return false;
        }
        if(this.user.id === undefined || !this.user.id){
            this.notify.error('Error','User Id missing. Please login again');
            this.prevStep();
            return false;
        }
        this.apiService.create('login_continue').save(this.user, (res, isSuccess) => {
            if (isSuccess) {
                this.state.bLoader = false;
                this.authService.setSession(res);
                this.router.navigate(['/home']);      
            }
        }, false);
    }

    forgetPassword() {
        this.selectedIndex = 2;
        this.fPControl.setValidators([Validators.required, Validators.minLength(3), Validators.email]);
        this.title = 'Forgot Password';
    }

    sendForgetPassword() {
        let scope = this;
        scope.apiService.create('forgotPassword/' + this.user.forgotPassword).get({}, (res, isSuccess) => {
            if (isSuccess) {
                if (typeof res.error != 'undefined') {
                    this.notify.error('Error!', res.error);
                    return;
                }
                this.notify.success('Success!', res.success);
                this.selectedIndex = 0;
            }
        }, false);
    }

    ngOnInit() {
        this.getStores();
    }

}

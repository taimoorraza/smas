import {Component, OnInit} from "@angular/core";
import { Router } from '@angular/router';

import {ApiService} from "./../services/api.service";
import {AuthService} from "./../services/auth.service";

@Component({
    template: "<div class='app-loader'>Please wait.. You are logging out.</div>"
})

export class LogoutComponent implements OnInit {

    constructor(public router: Router, public api: ApiService, public auth: AuthService) {
    }

    model = this.api.create('logout');
    logout() {
        this.model.save({}, (res, isSucess) => {
            if (isSucess) {
                this.auth.logout();
                this.router.navigate(['/login']);
            }
        });
    }
    ngOnInit() {
        this.logout();
    }
}
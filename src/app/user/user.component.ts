import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

import { NotificationsService } from 'angular2-notifications';
import { ApiService } from './../services/api.service';
import { ValidationService } from './../services/validation.service';
import { TemplateService } from './../services/template.service';
import { IUser, IUserRole, IStores } from './../interfaces';
import { AuthService } from "./../services/auth.service";


@Component({
    templateUrl: './user.html'
})

export class UserComponent implements OnInit {
    userForm: any;
    scenario: string = "Create";
    changePassword: boolean = false;
    createForm() {
        this.userForm = this.formBuilder.group({
            'name': ['', [Validators.required, ValidationService.allowAlphabet, ValidationService.hasWhiteSpace]],
            'username': ['', [Validators.required, ValidationService.hasWhiteSpace, Validators.pattern('^[A-Za-z0-9_\.-]+$')]],
            'roleId': ['', [Validators.required]],
            'statusId': ['', [ValidationService.customRequired]],
            'email': ['', [Validators.required, ValidationService.emailValidator, ValidationService.hasWhiteSpace]],
            'password': ['', [this.passwordRequired(this), Validators.minLength(6)]],
            'confirmPassword': ['', [this.passwordRequired(this), Validators.minLength(6), ValidationService.confirmPassValidator]]
        });
    }

    constructor(public apiService: ApiService, public notify: NotificationsService, public formBuilder: FormBuilder, public state: TemplateService, public authService: AuthService) {
        this.state.document = "User";
        this.createForm();
    }

    cPassword: string = '';
    dataIndex: number = 0;
    user: IUser = <IUser>{};
    data: IUser[] = [];
    public totalRecords: number;
    @ViewChild('dt') dt;
    roles: IUserRole[];
    status = [{ "id": "1", "name": "Enabled" }, { "id": "2", "name": "Disabled" }];
    divHidden: boolean = true;
    userId: number = this.authService.getSession('userId');
    public store: string = this.authService.getSession('store');

    onCreate(modal: any): void {
        let scope = this;
        this.user = <IUser>{};
        this.userForm.controls['statusId'].setValue("1");
        this.userForm.controls['roleId'].setValue("");
        this.user.store_id = this.authService.getSession('store_id');
        modal.show();
    }

    searchRecord(event) {
        let scope = this;
        let param:any;
        this.state.setDTParams(scope.dt, (res) => {
            param = res;
        })
        scope.getRecords(param);
    }

    onDelete(user: IUser): void {
        let scope = this;
        if (!confirm('Are you sure? You want to delete this record.')) {
            return;
        }
        scope.apiService.create('user').delete(user.id, {}, (res, isSuccess) => {
            if (isSuccess) {
                let param: any;
                let offset = (scope.data.length) - (scope.dt.first * 10);
                scope.notify.success('Success!', res.success);
                if (offset == 1)
                    scope.dt.first -= 1;
                else
                    scope.dt.filter = scope.dt.first;
                this.state.setDTParams(scope.dt, (res) => {
                    param = res;
                })
                scope.getRecords(param);
            }
        }, true, true);
    }

    onEdit(user: IUser, modal: any) {
        this.scenario = "Update";
        this.user = user;
        this.changePassword = true;
        modal.show();
    }

    onSave = function (user: IUser, modal: any) {
        let scope = this;
        if (scope.state.bLoader) {
            return false;
        }
        let afterSave = (res, isSuccess) => {
            if (isSuccess) {
                if (typeof res.error != 'undefined') {
                    scope.notify.error('Error', res.error);
                    return;
                }
                let param: any;
                scope.notify.success('Success', res.success);
                if(this.scenario == "Update"){
                    let offset = (scope.data.length) - (scope.dt.first * 10);
                    if (offset == 1)
                        scope.dt.first -= 1;
                    else
                        scope.dt.filter = scope.dt.first;
                }else{
                    scope.dt.first = 0;
                }
                this.state.setDTParams(scope.dt, (res) => {
                    param = res;
                })
                scope.getRecords(param);
                modal.hide();

            }
        }
        let notValidate: string[] = [];
        if (scope.scenario == "Update" && scope.changePassword) {
            notValidate = ['password', 'confirmPassword'];
        }
        if (!ValidationService.validateForm(scope.userForm, notValidate))
            return false;
        // Save data through service
        if (typeof user.id != 'undefined') {
            scope.apiService.create('user/save').update(user.id, user, afterSave);
        } else {
            scope.apiService.create('user/save').save(user, afterSave);
        }
    }

    getRecords(param): void {
        let scope = this;
        this.state.bLoader = true;
        scope.apiService.create('user').query(param,
            (res, isSuccess) => {
                if (isSuccess) {
                    scope.totalRecords = res.totalItems;
                    scope.data = res.items;
                }
                this.state.bLoader = false;
            }
        , true, true);
    }

    getRoles() {
        let scope = this;
        scope.apiService.create('roles').query({}, (res, isSuccess) => {
            if (isSuccess) {
                scope.roles = res;
            }
        }, true, true);
    }

    passwordRequired(obj) {
        return (control) => {
            if (obj.scenario != 'Create' && obj.changePassword) {
                return null;
            }
            return (ValidationService.isBlank(control.value) || (ValidationService.isString(control.value) && control.value.trim() == "") ?
                { "required": true } :
                null);
        }
    }

    showPassword() {
        this.changePassword = !this.changePassword;
    }

    ngOnInit(): void {
        this.getRoles();
    }

    loadDataLazy(event: any) {
        let param: any;
        this.state.setDTParams(event, (res) => {
            param = res;
        })
        this.getRecords(param);
    }
}
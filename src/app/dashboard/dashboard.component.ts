import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from './../services/auth.service';
import { ApiService } from './../services/api.service';
import { TemplateService } from './../services/template.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  	constructor(
      public router: Router,
	  	private notify: NotificationsService,
    	public auth: AuthService,
    	public api: ApiService,
    	public state: TemplateService,
  	) { 
      if(!this.auth.isLoggedIn()){
        this.router.navigate(['/login']);
      }
      this.state.document = "Dashboard";
    }

  	ngOnInit() {
  	}

}

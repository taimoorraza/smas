export interface IUser {
    id: number;
    name: string;
    email: string;
    password: string;
    role_id: number;
    store_id: number;
    status: number;
    created_at: Date;
    created_by: number;
    updated_at: Date;
    updated_by: number;
    is_deleted: boolean;
    username: string;
    userRoles: IUserRole;
    roleName: string;
}

export interface IUserRole {
    id: number;
    name: string;
    permission: any;
}

export interface IStores {
    id: number;
    name: string;
    code: string;
    city: string;
    address: string;
    created_at: Date;
    created_by: number;
    updated_at: Date;
    updated_by: number;
    is_deleted: boolean;
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { JwtModule } from '@auth0/angular-jwt';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { DataTableModule, MultiSelectModule, ProgressBarModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { AppComponent } from './app.component';
import { environment } from './../environments/environment';
// Interfaces
import { } from './interfaces';
// Services
import { routing } from "./app.routes";
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { AuthDirective } from './services/auth.directive';
import { AuthGuard } from './services/authguard.service';
import { TemplateService } from './services/template.service';
import { ValidationService } from './services/validation.service';
// Components
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { AsideComponent } from './shared/aside.component';
import { UserComponent } from './user/user.component';
// Directives
import { ControlMessages } from './shared/formError.component';

export function tokenGetter() {
  	let session = JSON.parse(localStorage.getItem('sm_session'));
  	if(session)
		  return session.token;
	return false
}

export class QSErrorHandler implements ErrorHandler {
  handleError(error) {
    // do something with the exception
    console.log(error);
    // location.href = 'error_page';
  }
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    LogoutComponent,
    AsideComponent,
    UserComponent,
    ControlMessages,
    AuthDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
  	ReactiveFormsModule,
  	BrowserAnimationsModule,
  	HttpModule,
  	RouterModule,
  	routing,
  	HttpClientModule,
  	SimpleNotificationsModule.forRoot(),
  	DataTableModule,
    TableModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    ProgressBarModule,
  	MultiSelectModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [environment.production?'':'localhost'],
        blacklistedRoutes: []
      }
    })
  ],
  providers: [
  	ApiService,
    AuthGuard,
    AuthService,
    TemplateService,
    ValidationService,
    { provide: ErrorHandler, useClass: QSErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

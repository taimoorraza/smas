import { Component,Output, EventEmitter, Renderer } from '@angular/core';

import { TemplateService } from "./../services/template.service";
import { AuthService } from "./../services/auth.service";


@Component({
    selector: 'aside',
    templateUrl: './aside.component.html',
})

export class AsideComponent {
    @Output() triggerChangePassword = new EventEmitter();
    public navsHeader: any[] = [
        {
            name: '<i class="icon fa fa-user"></i>&ensp;'+this.auth.getSession('username') + '<br /><i class="icon fa fa-building"></i>&ensp;' + this.auth.getSession('company')+'<br /><i class="icon fa fa-empire"></i>&ensp;'+ this.auth.getSession('role'),
            link: "",
            access: '',
            permission: '',
            visible: false,
            caret: 'fa-caret-down',
            childs: [
                {
                    name: "Change Password",
                    link: "",
                    icon: "icon fa fa-lock i-20",
                    active: false
                }
            ]
        }
    ]

    public navs: any[] = [
        {
            name: "Dashboard",
            link: "/home",
            access: '',
            permission: 'V',
            icon: "icon fa fa-dashboard i-20",
            childs: []
        },
        {
            name: "Costumer",
            link: "/customer",
            access: 'customer',
            permission: 'V',
            icon: "icon fa fa-user i-20",
            childs: []
        },       
        {
            name: "User Management",
            link: "",
            icon: "icon fa fa-users i-20",
            access: 'user_group',
            permission: 'V',
            visible: false,
            caret: 'fa-caret-down',
            childs: [
                {
                    name: "User",
                    link: "/user",
                    access: 'user',
                    permission: 'V',
                    active: false
                },
                {
                    name: "User Role",
                    link: "/user_role",
                    access: 'user_role',
                    permission: 'V',
                    active: false
                }
            ]
        },
        {
            name: "Logout",
            link: "/logout",
            icon: "icon fa fa-power-off i-20",
            permission: 'V',
            access: '',
            childs: []
        }
    ];

    showAside: boolean = false;

    constructor(public state: TemplateService, public auth: AuthService, public render: Renderer) {
    }

    changePassword(){
        this.triggerChangePassword.emit();
    }

    toggleAside() {
        this.showAside = !this.showAside;
        this.closeNavs();
        this.closeNavHeader();
        this.render.setElementStyle(document.getElementById('aside'), 'display', (this.showAside ? 'block' : 'block'));
        /*this.render.setElementClass(document.getElementById('aside'), 'in', this.showAside);*/
    }

    toggleHeaderMenu(i) {
        this.navsHeader[i].visible = !this.navsHeader[i].visible;
        this.navsHeader[i].caret = this.navsHeader[i].caret == 'fa-caret-down' ? 'fa-caret-up' : 'fa-caret-down';
    }

    toggleMenu(i) {
        this.navs[i].visible = !this.navs[i].visible;
        this.navs[i].caret = this.navs[i].caret == 'fa-caret-down' ? 'fa-caret-up' : 'fa-caret-down';
        this.closeNavs(i);
    }

    closeNavs(index?) {
        let i = typeof index != 'undefined' ? index : '';
        this.navs.forEach((ele, index) => {
            if (i != '') {
                if (index != i) {
                    ele.visible = false,
                        ele.caret = 'fa-caret-down'
                }
            } else {
                ele.visible = false,
                    ele.caret = 'fa-caret-down'
            }

        });
    }

    closeNavHeader(index?){
        let i = typeof index != 'undefined' ? index : '';
        this.navsHeader.forEach((ele, index) => {
            if (i != '') {
                if (index != i) {
                    ele.visible = false,
                        ele.caret = 'fa-caret-down'
                }
            } else {
                ele.visible = false,
                    ele.caret = 'fa-caret-down'
            }

        });
    }

    ngOnInit(){
        this.toggleAside();
    }
}
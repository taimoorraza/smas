<?php 

$app->get('/', 'ControllerUser:index');

/* * ************************** */
/* * ********** Setup ********* */
/* * ************************** */

$app->post('/login', 'ControllerUser:login');
$app->post('/login_continue', 'ControllerUser:loginContinue');
$app->get('/stores', 'ControllerUser:getStores');
$app->get('/roles', 'ControllerUser:getRoles');
$app->post('/logout', 'ControllerUser:logout');
/* * ************************** */
/* * ******* End Setup ******** */
/* * ************************** */

/* * ************************** */
/* * ******** Users ******** */
/* * ************************** */

$app->group('/user', function(){
    $controller = 'ControllerUser';

    $this->get('', $controller.':listing');
    $this->get('/country', $controller.':getCountries');
    $this->get('/zone', $controller.':getZones');
    $this->post('/delete/{id}', $controller.':deleteCustomer');
    $this->post('/save[/{id}]', $controller.':saveUser');
    $this->put('/save/{id}', $controller.':saveUser');
});

/* * ************************** */
/* * ****** End Customer ****** */
/* * ************************** */

/** 
 *  Generate database Interfaces for Angular 
 */
$app->get('/generateInterface', function(){
        $aResult = array();
        $aTables = Model::factory('User')
                    ->raw_query("SHOW TABLES FROM `smas`")->find_array();

        foreach($aTables as $key => $val){

            $val2 = explode('_', $val['Tables_in_smas']);
            foreach($val2 as $newVal){
                $new[$key][] = ucwords($newVal);

            }
            $aResult[$key]['table_original'] = $val['Tables_in_smas'];
            $aResult[$key]['table_name'] = implode('', $new[$key]);
        }

        foreach($aResult as $field){
            $aModel = Model::factory("User")
                        ->raw_query("SHOW FULL FIELDS FROM `".$field['table_original']."`")->find_array();

            foreach($aModel as $key => $val){
                $type = explode('(', $val['Type'], 2);

                switch($type[0]){
                    case 'int':
                    case 'tinyint':
                        $colType = 'number';
                    break;
                    case 'text':
                    case 'varchar':
                        $colType = 'string';
                    break;
                    case 'datetime':
                    case 'date':
                        $colType = 'Date';
                    break;
                    default;
                }
                if($val['Field'] == 'is_deleted'){
                    $colType = 'boolean';
                }
                $aResult[$field['table_original']]['columns'][] = $val['Field'].': '.$colType.';';
                $aResult[$field['table_original']]['alternate_name'] = $field['table_name'];
            }

        }

        foreach($aResult as $key => $res){
            if(!is_int($key)){

            echo 'export interface '.$res['alternate_name'].' {<br>';
            foreach($res['columns'] as $col){
                echo "&nbsp;&nbsp;&nbsp;&nbsp;".$col."<br>";
            }
            echo '}<br>';
            }
        }
});
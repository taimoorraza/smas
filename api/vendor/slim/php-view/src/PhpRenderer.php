<?php
/**
 * Slim Framework (http://slimframework.com)
 *
 * @link      https://github.com/slimphp/PHP-View
 * @copyright Copyright (c) 2011-2015 Josh Lockhart
 * @license   https://github.com/slimphp/PHP-View/blob/master/LICENSE.md (MIT License)
 */
namespace Slim\Views;

use \InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class PhpRenderer
 * @package Slim\Views
 *
 * Render PHP view scripts into a PSR-7 Response object
 */
class PhpRenderer
{
    /**
     * @var string
     */
    protected $templatePath;

    /**
    * The default layout template.
    */
    const DEFAULT_LAYOUT = 'layout.php';

    /**
    * The data key to use for the layout template.
    */
    const LAYOUT_KEY = 'layout';

    /**
    * The data key to use for yielded content.
    */
    const YIELD_KEY = 'yield';

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var object
     */
    public $container;

    /**
     * @var object
     */
    protected $document;

    /**
     * SlimRenderer constructor.
     *
     * @param string $templatePath
     * @param array $attributes
     */
    public function __construct($container, $templatePath = "", $attributes = [])
    {
        $this->container = $container;
        $this->document = $container->get('document');
        $this->templatePath = rtrim($templatePath, '/\\') . '/';
        $this->attributes = $attributes;
    }

    /**
     * Render a template
     *
     * $data cannot contain template as a key
     *
     * throws RuntimeException if $templatePath . $template does not exist
     *
     * @param ResponseInterface $response
     * @param string             $template
     * @param array              $data
     *
     * @return ResponseInterface
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render(ResponseInterface $response, $template, array $data = [])
    {   
        $layout = $this->getLayout($data);
        $output = $this->fetch($template, $data);
        if (is_string($layout)) {
            $output = $this->renderLayout($layout, $output, $data);
        }
        $response->getBody()->write($output);

        return $response;
    }

    /**
    * Returns the layout for this view. This will be either
    * the 'layout' data value, the applications 'layout' configuration
    * value, or 'layout.php'.
    *
    * @param array $data Any additonal data to be passed to the template.
    *
    * @return string|null
    */
    public function getLayout($data = null) {
        $layout = null;

        // 1) Check the passed in data
        if (is_array($data) && array_key_exists(self::LAYOUT_KEY, $data)) {
            $layout = $data[self::LAYOUT_KEY];
            unset($data[self::LAYOUT_KEY]);
        }

        // 2) Check the data on the View
        if ($this->getAttribute(self::LAYOUT_KEY)) {
            $layout = $this->getAttribute(self::LAYOUT_KEY);
            $this->remove(self::LAYOUT_KEY);
        }

        // 3) Check the Slim configuration
        if (is_null($layout)) {
            if(isset($this->container->get('settings')[self::LAYOUT_KEY])){
                $layout = $this->container->get('settings')[self::LAYOUT_KEY];                
            }
        }

        // 4) Use the default layout
        if (is_null($layout)) {
            $layout = self::DEFAULT_LAYOUT;
        }

        return $layout;
    }

    protected function renderLayout($layout, $yield, $data = null) {
        if (!is_array($data)) {
            $data = array();
        }
        $data[self::YIELD_KEY] = $yield;
        $currentTemplate = $this->templatePath;
        $result = $this->fetch($layout, $data);
        $this->templatePath = $currentTemplate;

        return $result;
    }

    /**
     * Get the attributes for the renderer
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set the attributes for the renderer
     *
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Set the attributes for the renderer
     *
     * @param array $attributes
     */
    public function appendAttributes(array $attributes)
    {
        $this->attributes = array_merge($this->attributes,$attributes);
    }

    /**
     * Add an attribute
     *
     * @param $key
     * @param $value
     */
    public function addAttribute($key, $value) {
        $this->attributes[$key] = $value;
    }

    /**
     * Retrieve an attribute
     *
     * @param $key
     * @return mixed
     */
    public function getAttribute($key) {
        if (!isset($this->attributes[$key])) {
            return false;
        }

        return $this->attributes[$key];
    }

    /**
     * Get the template path
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Set the template path
     *
     * @param string $templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = rtrim($templatePath, '/\\') . '/';
    }

    /**
     * Renders a template and returns the result as a string
     *
     * cannot contain template as a key
     *
     * throws RuntimeException if $templatePath . $template does not exist
     *
     * @param $template
     * @param array $data
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function fetch($template, array $data = []) {
        if (isset($data['template'])) {
            throw new \InvalidArgumentException("Duplicate template key found");
        }

        if (!is_file($this->templatePath . $template)) {
            throw new \RuntimeException("View cannot render `$template` because the template does not exist");
        }

        $data = array_merge($this->attributes, $data);

        try {
            ob_start();
            $this->protectedIncludeScope($this->templatePath . $template, $data);
            $output = ob_get_clean();
        } catch(\Throwable $e) { // PHP 7+
            ob_end_clean();
            throw $e;
        } catch(\Exception $e) { // PHP < 7
            ob_end_clean();
            throw $e;
        }

        return $output;
    }

    /**
    * Unsets the data for the given key.
    *
    * @param string $key
    */
    public function remove($key) {
        if (!isset($this->attributes[$key])) {
            return false;
        }

        unset($this->attributes[$key]);
    }

    /**
     * @param string $template
     * @param array $data
     */
    protected function protectedIncludeScope ($template, array $data) {
        extract($data);
        include $template;
    }
}

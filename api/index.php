<?php
error_reporting(0);
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}
define('CURR_DIR',  '/');
define('SESSION_NAME',  'SMsession');

require_once 'vendor/php-jwt/src/JWT.php';
require 'vendor/autoload.php';
require_once 'vendor/library/Validator.php';
require_once 'vendor/library/idiorm.php';
require_once 'vendor/library/paris.php';
//require_once 'vendor/PHPMailer/class.phpmailer.php';
require_once 'tcpdf_config.php';
require_once 'vendor/library/CHtml.php';
require_once 'lib/Setting.php';
require_once 'db_config.php';
require_once 'constants.php';

require_once 'lib/CommonHelper.php';
require_once 'lib/document.php';
require_once 'lib/currency.php';

$settings = require 'settings.php';
$app = new \Slim\App($settings);

require_once 'lib/dependencies.php';

require_once 'lib/Plinq.php';
initSession(SESSION_NAME);


function my_autoloader($className) {
    $arr = preg_split('/(?=[A-Z])/', $className);
    $arr = array_slice($arr, 1);
    $path = false;
    if (stristr($className, 'Controller')) {
        $path = __DIRNAME__ . "/controllers/" . $className . ".php";
    } else {
        $path = __DIRNAME__ . "/models/" . $className . ".php";
    }
    if (file_exists($path)) {
        require_once $path;
    }
}

spl_autoload_register("my_autoloader");

// Register middleware
require_once 'lib/middleware.php';

// Register routes
require_once 'routes.php';

$app->run();
?>
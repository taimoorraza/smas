<?php

class User extends Model {

    public static $_table = 'user';

    //validation rules
    /*protected $_rules = array(
        'insert|update' => array(
            'rules' => array(
                'email' => array('required' => true),
                'role_id' => array('required' => true),
            ),
    ));*/

    /*public function beforeValidate() {
        $bResult = parent::beforeValidate();
        if ($this->getScenario() == 'insert') {
            $iCount = ORM::for_table(self::$_table)->where('email', $this->email)->where('is_deleted',0)->count();
            $uCount = ORM::for_table(self::$_table)->where('username', $this->username)->where('is_deleted',0)->count();
            if ($iCount > 0) {
                $this->addError('email', 'Duplicate Entry, email already exists');
            }
            if ($uCount > 0) {
                $this->addError('username', 'Duplicate Entry, Username already exists');
            }
        }

        return $bResult;
    }

    function authenticate($data) {
        try {
            if (trim($data['name']) == "") {
                throw new Exception("Invalid Username");
            }
            if (trim($data['password']) == "") {
                throw new Exception("Invalid Password Entered");
            }

           $oModel = Model::factory('User')->where('name', $data['name'])
               ->where('password',md5($data['password']))
                ->find_one();

            if (!$oModel) {
                throw new Exception("No User found");
            }

            $oRole = $oModel->getUserRole()->find_one();
            $aResult = $oModel->as_array();
            $aResult['role'] = $oRole->role_name;

            $_SESSION['user_id'] = $aResult['id'];
            $_SESSION['email'] = $aResult['email'];
            $_SESSION['name'] = $aResult['name'];
            $_SESSION['last_login'] = $aResult['last_login'];
            $_SESSION['role_id'] = $aResult['role_id'];
            $_SESSION['role'] = $aResult['role'];
            $_SESSION['aPermissions'] = $oRole->getRolePermissions();
          //  $_SESSION['denied_routes'] = $this->getInaccessibleRoutes($oRole->id);
        } catch (Exception $ex) {
            $aResult['error'] = $ex->getMessage();
        }
        return $aResult;
    }*/

    function getUserRole() {
        return $this->belongs_to('UserRole', 'role_id');
    }

}

?>
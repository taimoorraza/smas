<?php

class UserSession extends Model {

    public static $_table = 'user_session';


    public function beforeSave() {
        parent::beforeSave();
        d($this->as_array(),0,0,1);
    }

    public function createSession($aResult) {
        $time = $aResult['remember_me'] ? 2628000 : 2628000; //1 month or 60 mins
        $userSession = Model::factory('UserSession')->create();
        $sessionTokenExpired = date('Y-m-d H:i:s', time() + $time);

        $userSession->user_id = $aResult['user_id'];
        $userSession->store_id = $aResult['store_id'];
        $userSession->token_expired = $sessionTokenExpired;
        $userSession->data = json_encode($aResult);
        $userSession->created_on = date('Y-m-d H:i:s');
        $userSession->updated_on = date('Y-m-d H:i:s');
        
        $userSession->save();

        $payload = array("user_sessionId" => $userSession->id);
        $token = JWT::encode($payload, AUTHORIZATION_KEY);

        return $token;
    }
}

?>
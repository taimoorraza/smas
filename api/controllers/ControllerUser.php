<?php 
class ControllerUser extends BaseController {

    public function index($req, $resp, $args){
        echo "API is working";
    }


    public function login($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aPost = $req->getParsedBody();
            $oModel = Model::factory('User')
                        ->where_raw("(email ='".$aPost['email']."' OR username = '".$aPost['email']."')")
                        ->where('password', md5($aPost['password']))
                        ->find_one();
            if(!$oModel){
                throw new Exception("Email or password is incorrect");
            }

            if($oModel->status != 1)
                throw new Exception("Your account has been disabled. Please contact admin");
            $aResult['id'] = $oModel->id;
        }
        catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function loginContinue($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aPost = $req->getParsedBody();
            if(isset($aPost['id'])){
                $oModel = Model::factory('User')
                    ->table_alias('u')
                    ->select_many('u.*','r.permission', array('role'=>'r.name', 'store'=>'s.name'))
                    ->inner_join('user_role','u.role_id=r.id','r')
                    ->inner_join('store','u.store_id=s.id','s')
                    ->where('u.id',$aPost['id'])
                    ->find_one();
            }else{
                throw new Exception("User Id is missing. Please login again");
            }
            if(!$oModel){
                throw new Exception("User Id is missing. Please login again");
            }
            $aResult = $oModel->as_array();
            $aResult['user_id'] = $oModel->id;
            $aResult['company'] = $this->app->siteConfig->get('company_name');
            $aResult['permission'] = json_decode($aResult['permission'],true);
            $aResult['store_id'] = $aPost['store'];
            unset($aResult['password']);
            $token = Model::factory('UserSession')->create()->createSession($aResult);
            $aResult['token'] = $token;
        }
        catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function logout($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $auth = checkSession();
            if($auth)
                $auth->delete();
            else
                throw new Exception("No token found");
        }catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function getStores($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aResult = Model::factory('Store')->find_array();
        }catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function getRoles($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aResult = Model::factory('UserRole')->find_array();
        }catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function listing($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aParams = $req->getQueryParams();
            $aModel = Model::factory('User')
                    ->table_alias('u')
                    ->select_many('u.id', 'u.email','u.name','u.username','u.role_id','u.store_id','u.status',array('roleName'=> 'r.name', 'storeName'=>'s.name'))
                    ->join('user_role','u.role_id = r.id','r')
                    ->join('store','u.store_id = s.id','s')
                    ->where('u.is_deleted',0)
                    ->where('s.is_deleted',0);
            if($aParams){
                if(isset($aParams['search']) && $aParams['search']){
                    $aModel->where_raw(" (LOWER(u.name) LIKE '%".$aParams['search']."%' OR LOWER(u.username) LIKE '%".$aParams['search']."%' OR LOWER(s.name) LIKE '%".$aParams['search']."%' OR LOWER(r.name) LIKE '%".$aParams['search']."%' OR LOWER(CASE WHEN u.`status` IS NULL OR u.`status` = 0 THEN 'Disabled' ELSE 'Enabled' END) LIKE '%".$aParams['search']."%')");
                }
                $oCount = clone $aModel;
                if(isset($aParams['limit']) && $aParams['limit'] && isset($aParams['page']) && $aParams['page']){
                    $aModel->offset(($aParams['page']-1)*$aParams['limit']);
                    $aModel->limit($aParams['limit']);
                }
            }
            if(isset($oCount) && $oCount)
                $aResult['totalItems'] = $oCount->count();    
            $aResult['items'] = $aModel->find_array();
            //d(array(ORM::get_last_query(),$aResult),1);
        }catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }

    public function saveUser($req, $resp, $args){
        $aResult = array();
        $code = 200;
        try {
            $aPost = $req->getParsedBody();
            if(isset($args['id']))
                $id = $args['id'];
            if(isset($id))
                $oModel = Model::factory('User')->find_one($id);
            else
                $oModel = Model::factory('User')->create();
            $oModel->setFields($aPost);
            if(isset($aPost['password']))
                $oModel->password = md5($aPost['password']);
            $oModel->save();
            if($oModel->hasErrors()){
                throw new Exception("Error on saving user");
            }
            $aResult['success'] = "User has been ".(isset($id)?'updated':'created')." successfully";
        }catch(Exception $ex) {
            $code = 500;
            $aResult['error'] = $ex->getMessage();
        }
        return $resp->withJson($aResult, $code);
    }
  
}
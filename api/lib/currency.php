<?php
final class currency
{
    
    private $_sCode;
    private $_aCurrencies = array();
    private $_app;
    
    public function __construct($container) {
        $this->_app = $container;
    }

    public function init(){
        $aModels = ORM::for_table('currency')->find_array();
        if ($aModels) {
            
            foreach ($aModels as $aModel) {

                $this->_aCurrencies[$aModel['code']] = $aModel;
            }
            $this->_aCurrencies;
        }
        $oModel = ORM::for_table('currency')->where('currency_id', $this->_app->siteConfig->get('config_tax_currency_id'))->find_one();
        $this->_sCode = $oModel->code;
    }
    
    public function format($number, $currency = '', $value = '', $format = TRUE) {
        if(!$this->_sCode){
            $this->init();
        }
        if ($currency && $this->has($currency)) {
            $symbol_left = $this->_aCurrencies[$currency]['symbol_left'];
            $symbol_right = $this->_aCurrencies[$currency]['symbol_right'];
            $decimal_place = $this->_aCurrencies[$currency]['decimal_place'];
        } else {
            $symbol_left = $this->_aCurrencies[$this->_sCode]['symbol_left'];
            $symbol_right = $this->_aCurrencies[$this->_sCode]['symbol_right'];
            $decimal_place = $this->_aCurrencies[$this->_sCode]['decimal_place'];
            
            $currency = $this->_sCode;
        }
        
        if ($value) {
            $value = $value;
        } else {
            $value = $this->_aCurrencies[$currency]['value'];
        }
        
        $value = $number;
        
        $string = '';
        
        if (($symbol_left) && ($format)) {
            $string.= $symbol_left;
        }
        
        if ($format) {
            $decimal_point = '.';
        } else {
            $decimal_point = '.';
        }
        
        if ($format) {
            $thousand_point = ',';
        } else {
            $thousand_point = '';
        }
        
        $string.= number_format(round($value, (int)$decimal_place) , (int)$decimal_place, $decimal_point, $thousand_point);
        
        if (($symbol_right) && ($format)) {
            $string.= $symbol_right;
        }
        
        return $string;
    }
    
    public function convert($value, $from, $to) {
        if(!$this->_sCode){
            $this->init();
        }
        if (isset($this->_aCurrencies[$from])) {
            $from = $this->_aCurrencies[$from]['value'];
        } else {
            $from = 0;
        }
        
        if (isset($this->_aCurrencies[$to])) {
            $to = $this->_aCurrencies[$to]['value'];
        } else {
            $to = 0;
        }
        
        return $value * ($to / $from);
    }
    
    public function getSymbol($currency = '') {
        if(!$this->_sCode){
            $this->init();
        }
        if (!$currency) {
            $currency = $this->getCode();
        }
        $aResult = array(
            '',
            0
        );
        if (isset($this->_aCurrencies[$currency])) {
            if ($this->_aCurrencies[$currency]['symbol_left'] != "") {
                $aResult[0] = $this->_aCurrencies[$currency]['symbol_left'];
                $aResult[1] = 0;
            } else if ($this->_aCurrencies[$currency]['symbol_right'] != "") {
                $aResult[0] = $this->_aCurrencies[$currency]['symbol_right'];
                $aResult[1] = 1;
            }
        }
        return $aResult;
    }
    
    public function getCode() {
        return $this->_sCode;
    }
    
    public function getValue($currency = '') {
        // if (!$currency) {
        //     return $this->_aCurrencies[$this->getCode()]['value'];
        // } elseif ($currency && isset($this->_aCurrencies[$currency])) {
        //     return $this->_aCurrencies[$currency]['value'];
        // } else {
        //     return 0;
        // }
        return 1;
    }
    
    public function has($currency) {
        return isset($this->_aCurrencies[$currency]);
    }
}
?>
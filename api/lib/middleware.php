<?php
// Application middleware

// Middleware Authorization for Customer
// To apply the middleware need to add it on route level
// Ex: $app->route('/','Controller:method')->add('middleware');
$authenticate = function ($req, $res, $next)  {

        try {
            $userSession = checkSession();

            if($userSession === false){
                throw new Exception("Session expired", 401);
            }
            
        } catch (Exception $ex) {
            $code = 500;
            if($ex->getCode()){
                $code = $ex->getCode();
            }
            return $res->withJson(array('message' => $ex->getMessage()), $code);
        }
        $res = $next($req, $res);
        return $res;
};

// Middleware Authorization for Service Provider
// To apply the middleware need to add it on route level
// Ex: $app->route('/','Controller:method')->add('middleware');
$authenticateProvider = function ($req, $res, $next)  {
        try {
            if (!isset($_SESSION['is_provider_login']) || $_SESSION['is_provider_login'] == '') {
                throw new Exception('Login Required', 1);
            }
        } catch (Exception $ex) {
            $this->flash('errors', $ex->getMessage());
            if ($ex->getCode() == 1) {
                $res->withRedirect(CURR_DIR);
            } else {
                $res->withRedirect(CURR_DIR);
            }
        }
        $res = $next($req, $res);
        return $res;
};

// Middleware to initiate and pass vairables to layout
$app->add(function($req, $res, $next){
    $request_url = $req->getUri();
    if (preg_match('/\/[\d]+/', $request_url)) {
        $request_url = preg_replace('/\/[\d]+/', '', $request_url);
    }
    $aData = array();
    if(isset($_SESSION['success']) && $_SESSION['success']){
        $aData['flash']['success'] = $_SESSION['success'];
       // d($aData,1);
        unset($_SESSION['success']);
    }

    $this->request_url = $request_url;

    $aData['isProverLogged'] = false;

    $this->view->appendAttributes($aData);
    return $next($req, $res);
});
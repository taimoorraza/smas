<?php

function isSecure() {
    return
        (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
        || $_SERVER['SERVER_PORT'] == 443;
}

/** 
 * Please Update base_url in hybrid_config if changing directory from /dbohra/ to anything
 */
define("__DIRNAME__", __DIR__);
define('MAIN_DIR',  '/smas/');
define('HTTP_MAIN', $_SERVER['HTTP_HOST'] . MAIN_DIR);
define('HTTP_ROOT', $_SERVER['HTTP_HOST'] . CURR_DIR);
define('HTTP_SERVER', (isSecure() ? 'https' : 'http') . '://'  . HTTP_ROOT);
define('HTTP_MAIN_SERVER', (isSecure() ? 'https' : 'http') . '://' . HTTP_MAIN);
define('DIR_FILES',__DIRNAME__  . '/files/');
define('HTTP_FILES',(isSecure() ? 'https' : 'http') . '://' . HTTP_MAIN . 'api/files/');
define('DIR_LANG',__DIRNAME__  . '/language/');
define('DATE_FORMAT', 'd/m/Y h:i a');
define('EMAIL_TEMPLATE_DIR', __DIRNAME__.'/templates/email/');


// SMTP Details
define("MAIL_SMTP", true);
define("SMTP_HOST", "mail.q-sols.com");
define("SMTP_USER", "sms@q-sols.com");
define("SMTP_PASS", "sms110786");

// EMail Details
define("ADMIN_EMAIL", "sms@q-sols.com");
define("ADMIN_NAME", "q-pos");

define('AUTHORIZATION_KEY', 'sM10oCT20!8');